#!/usr/bin/env python

from tornado.options import parse_command_line, options, define
from tornado.ioloop import IOLoop
import tornado.httpserver
import tornado.web

import signal
from logging import getLogger
from broker.access_control import SinglePasswordAuthentication, NoAuthentication, FileAuthentication, WebAuthentication
from broker.persistence import InMemoryPersistence, RedisPersistence

from broker.server import MQTTServer
from broker.tasks import start_purge_staled_sessions_task
from broker.subscription import SubscriptionTree
from broker.websocket import MqttWebSocket

define('rhost', 'localhost', str, "Redis host address")
define('rport', 6379, int, "Redis host port")
define('rpassword', None, str, "Redis password")
define('redis', False, bool, "Use redis as queue backend")

define('websockets', False, bool, "Use WebSockets")
define('wsport', 80, int, "Network port to listen for WebSocket connections")
define('wssport', 433, int, "Network port to listen for WebSocket Secure connections")

define('ssl', False, bool, "Use SSL/TLS on socket")
define('sslkey', None, str, "SSL/TLS Key file path")
define('sslcert', None, str, "SSL/TLS Certificate file path")

define('authfile', None, str, "Authentication and authorization config file path")
define('webauth', None, str, "Authentication and authorization web API address")
define('password', None, str, "Password for client authentication")
define('log_payloads', False, bool, "Log payload content")


class OsSignalHandler():
    def __init__(self, log):
        self.log = log
        self.servers = list()

        # handle interrupts
        signal.signal(signal.SIGINT, self.handle_signal)
        # Handle unix kill command
        signal.signal(signal.SIGTERM, self.handle_signal)

    def add(self, server):
        self.servers.append(server)

    def handle_keyboard_interrupt(self):
        self._beautifully_exit()

    def handle_signal(self, *args):
        self._beautifully_exit()

    def _beautifully_exit(self):
        """ Handles client disconnection before stopping the broker. """
        print("Disconnecting clients")
        self.log.info('disconnecting clients')

        for server in self.servers:
            server.disconnect_all_clients()

        IOLoop.instance().stop()


def create_redis_persistence(options, log):
    import redis
    redis_client = redis.StrictRedis(
        host=options.rhost,
        port=options.rport,
        password=options.rpassword
    )

    print("[option] Using redis at (%s, %d)" %
          (options.rhost, options.rport))
    log.info("[option] Using redis at (%s, %d)" %
          (options.rhost, options.rport))

    return RedisPersistence(redis_client)


def start_mqtt_server(persistence, clients, subscriptions,
                      authentication_agent, log):
    server = MQTTServer(authentication=authentication_agent,
                        persistence=persistence,
                        clients=clients,
                        subscriptions=subscriptions,
                        ssl_options=None)

    server.listen(1883)
    print("MQTT listening port 1883")
    log.info("MQTT listening port 1883")

    return server


def start_secure_mqtt_server(persistence, clients, subscriptions,
                             authentication_agent, log):
    if options.sslkey and options.sslcert:
        import ssl
        ssl_options = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_options.load_cert_chain(options.sslcert, options.sslkey)
        print("[option] Using SSL/TLS")
        log.info("[option] Using SSL/TLS")

    else:
        print("[option] To use SSL/TLS both sslkey and sslcert must be defined")
        log.info("[option] To use SSL/TLS both sslkey and sslcert must be defined")
        ssl_options = None

    server = MQTTServer(authentication=authentication_agent,
                        persistence=persistence,
                        clients=clients,
                        subscriptions=subscriptions,
                        ssl_options=ssl_options)

    server.listen(8883)
    print("Secure MQTT listening port 8883")
    log.info("Secure MQTT listening port 8883")

    return server


def start_websockets_server(log):
    application = tornado.web.Application([
        (r'/mqtt', MqttWebSocket),
    ])

    port = options.wsport

    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(port)

    print("WebSockets listening port {}".format(port))
    log.info("WebSockets listening port {}".format(port))

    return http_server


def start_secure_websockets_server(log):
    if options.sslkey and options.sslcert:
        import ssl
        ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_ctx.load_cert_chain(options.sslcert, options.sslkey)

        print("[option] Using WebSockets with SSL/TLS")
        log.info("[option] Using WebSockets with SSL/TLS")

    else:
        print("[option] To use SSL/TLS both sslkey and sslcert must be defined")
        log.info("[option] To use SSL/TLS both sslkey and sslcert must be defined")
        ssl_ctx = None

    application = tornado.web.Application([
        (r'/mqtt', MqttWebSocket),
    ])

    port = options.wssport

    http_server = tornado.httpserver.HTTPServer(application, ssl_options=ssl_ctx)
    http_server.listen(port)

    print("Secure WebSockets listening port {}".format(port))
    log.info("Secure WebSockets listening port {}".format(port))

    return http_server


def get_persistence(options, log):
    if options.redis:
        print('persistence: redis')
        log.info('persistence: redis')
        return create_redis_persistence(options, log)
    else:
        print('persistence: memory')
        log.info('persistence: memory')
        return InMemoryPersistence()


def get_authentication_agent(options, log):
    if options.authfile is not None:
        print("auth: file")
        log.info("authentication agent: file authentication")
        return FileAuthentication(options.authfile)

    elif options.webauth is not None:
        print("auth: web")
        log.info("authentication agent: web authentication")
        return WebAuthentication(options.webauth)

    elif options.password is not None:
        print("auth: single pw authentication")
        log.info("authentication agent: single password authentication")
        return SinglePasswordAuthentication(options.password)

    else:
        print("auth: no authentication")
        log.info("authentication agent: no authentication")
        return NoAuthentication()


def main():
    log = getLogger('activity.broker')
    log.info('starting broker')

    subscriptions = SubscriptionTree()
    persistence = get_persistence(options, log)
    authentication_agent = get_authentication_agent(options, log)

    signal_handler = OsSignalHandler(log)

    clients = dict()
    servers = list()

    log.info('starting server')
    server = start_mqtt_server(persistence, clients, subscriptions,
                               authentication_agent, log)

    signal_handler.add(server)
    servers.append(server)

    if options.ssl:
        print("starting secure server")
        log.info('starting secure server')
        sserver = start_secure_mqtt_server(persistence, clients, subscriptions,
                                           authentication_agent,
                                           log)

        signal_handler.add(sserver)
        servers.append(sserver)

    else:
        sserver = None

    MqttWebSocket.target_server = server

    if options.websockets:
        start_websockets_server(log)

        if options.ssl:
            start_secure_websockets_server(log)

    start_purge_staled_sessions_task(clients, servers)

    try:
        print("MQTT-Broker Started")
        log.info('broker started')
        IOLoop.instance().start()

    except KeyboardInterrupt:
        signal_handler.handle_keyboard_interrupt()

    print("MQTT-Broker Stopped")
    log.info('broker stopped')


parse_command_line()
main()
