##############################################
Welcome to mqtt-broker's Project Documentation
##############################################

This project was originally developed **Tegris Ltda** for the FieldLink_
application and is distributed under the conditions described in the
:doc:`licensing chapter </licensing>`.

You may contact the project maintainer by dropping a message at
`@rafgoncalves <http://www.twitter.com/rafgoncalves>`_ or using the
`issue tracker`_.

#################
Table of Contents
#################

.. toctree::
   :maxdepth: 3

   get_started
   frontend
   monitoring
   advanced_configuration
   contribute
   roadmap
   references
   licensing
   sandbox


.. _FieldLink: https://www.fieldlink.me
.. _issue tracker: https://bitbucket.org/tegris/mqtt-broker/issues