
class close_connection_on_exit(object):
    """
    Decorator to force connection close. To be used specifically with
    `MQTTClient._process_incoming_packets` and `MQTTClient._process_outgoing_packets`
    """
    def __init__(self, f):
        self.f = f

    def __call__(self, client, connection):
        with self.Context(client, connection):
            yield from self.f(client, connection)

    class Context(object):
        def __init__(self, client, connection):
            self.client = client
            self.connection = connection

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            if self.client.is_connected():
                self.client.disconnect()

            if not self.connection.closed():
                self.connection.close()

            self.connection.force_close_stream()
