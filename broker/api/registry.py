import logging

logger = logging.getLogger('activity.api')


"""
A dictionary used to relate the API topics to functions that handles the operations.
"""
REGISTERED_APIS = {}


class register_api(object):
    """
    Decorator that does a simple registration of a function on `REGISTERED_APIS`.
    """
    def __init__(self, api_topic):
        self.api_topic = api_topic

    def __call__(self, f):
        if self.api_topic in REGISTERED_APIS:
            raise ValueError('duplicate API topic')

        REGISTERED_APIS[self.api_topic] = f
        return f
