import json
import logging
import os

from importlib import import_module

from .registry import REGISTERED_APIS

logger = logging.getLogger('activity.api')

# The topic prefix of System APIs
SYS_API_PREFIX = "$SYS/"


def call_api(message, client, server):
    if not message.topic.startswith(SYS_API_PREFIX):
        raise ValueError('not a System API topic')

    try:
        function = REGISTERED_APIS[message.topic]
        logger.debug(u"executing API on {}".format(message.topic))

    except KeyError:
        logger.debug(u"API not found on {}".format(message.topic))
        return

    try:
        data = json.loads(message.payload.decode("utf-8"))

    except (AttributeError, TypeError, json.decoder.JSONDecodeError):
        logger.warning(u"error reading payload")
        return

    try:
        function(data, client, server)

    except Exception:
        logger.warning("unhandled exception calling api on {}".format(message.topic),
                       exc_info=True)


# API MODULES LOADING
# ==============================================================================

for module in os.listdir(os.path.dirname(__file__)):
    if module in ['__init__.py', 'registry.py'] or module[-3:] != '.py':
        continue

    import_module('.' + module[:-3], __package__)

del module
