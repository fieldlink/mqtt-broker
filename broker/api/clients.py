import logging

from .registry import register_api

logger = logging.getLogger('activity.api')


@register_api("$SYS/clients/subscribe")
def subscribe_client(data, origin_client, server):
    """
    Subscribes a client to one or more topics.

        {
            "clientid": "CLIENT ID",
            "topics": ["subscriptionmask1", "subscriptionmask2", ...],
            "qos": 1
        }
    """
    clientid = data.get("clientid")
    topics = data.get("topics")
    qos = data.get("qos")

    if not clientid or clientid not in server.clients:
        logger.warning(u"[uid: {}] client not found".format(clientid or '?'))
        return

    if not topics or not isinstance(topics, list):
        logger.warning(u"[uid: {}] invalid topics: {}".format(clientid, topics))
        return

    if not isinstance(qos, int):
        logger.warning(u"[uid: {}] invalid qos value: {}".format(clientid, qos))
        return

    logger.info(u"subscribing topics for client {}".format(clientid or '[EMPTY]'))

    client = server.clients[clientid]

    for topic in topics:
        if isinstance(topic, str):
            client.subscribe(topic, qos)

        else:
            logger.warning(u"[uid: {}] invalid topic: {}".format(clientid, topic))


@register_api("$SYS/clients/unsubscribe")
def unsubscribe_client(data, origin_client, server):
    """
    Removes subscriptions of a client.

    To remove specific subscriptions:

        {
            "clientid": "CLIENT ID",
            "topics": ["subscriptionmask1", "subscriptionmask2", ...]
        }

    To remove all subscriptions:

        { "clientid": "CLIENT ID", "topics": "*" }

    """
    clientid = data.get("clientid")
    topics = data.get("topics")

    if not clientid or clientid not in server.clients:
        logger.warning(u"[uid: {}] client not found".format(clientid or '[EMPTY]'))
        return

    if isinstance(topics, list):
        logger.info(u"unsubscribing topics for client {}".format(clientid or '[EMPTY]'))
        client = server.clients[clientid]

        for topic in topics:
            if isinstance(topic, str):
                client.unsubscribe(topic)

            else:
                logger.warning(u"[uid: {}] invalid topic: {}".format(clientid, topic))

    elif topics == "*":
        client = server.clients[clientid]
        client.unsubscribe_all()

    else:
        logger.warning(u"[uid: {}] invalid topics: {}".format(clientid, topics))


@register_api("$SYS/clients/clear_outgoing_queue")
def clear_outgoing_queue(data, origin_client, server):
    """
    Clears the outgoing queue of a client. All the messages with pending
    transmission to the Client will be permanently lost.

        { "clientid": "CLIENT ID" }
    """
    clientid = data.get("clientid")

    if clientid and clientid in server.clients:
        logger.info(u"clearing outgoing queue for client {}".format(clientid or '[EMPTY]'))

        server.disconnect_clientid(clientid)

        client_persistence = server.persistence.get_for_client(clientid)
        client_persistence.outgoing_publishes.clear()

    else:
        logger.warning(u"[uid: {}] client not found".format(clientid or '?'))


@register_api("$SYS/clients/clean_session")
def clean_session(data, origin_client, server):
    """
    Does a full session cleanup, as if a client with clean session flag
    disconnected.

    The payload should have JSON format, with the following pattern:

        { "clientid": "CLIENT ID" }
    """
    clientid = data.get("clientid")

    if clientid and clientid in server.clients:
        logger.info(u"cleaning session for client {}".format(clientid or '[EMPTY]'))

        server.disconnect_clientid(clientid)
        server.remove_clientid(clientid)

    else:
        logger.warning(u"[uid: {}] client not found".format(clientid or '?'))
