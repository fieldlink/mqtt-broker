import logging

from collections import namedtuple
from itertools import zip_longest

from broker.exceptions import SubscriptionError


logger = logging.getLogger('activity.subscriptions')


class SubscriptionWildcards:
    SINGLE_LEVEL = '+'
    MULTI_LEVEL = '#'


class SubscriptionTree:
    SYS_SPECIAL_CHAR = '$'
    TOPIC_SEPARATOR = '/'

    def __init__(self):
        self.sys_root_node = SubscriptionNode()
        self.root_node = SubscriptionNode()

    def add_subscription(self, clientid, topic, qos):
        _validate_subscription_mask(topic)

        subscription = Subscription(clientid, topic, qos)

        topic_parts, topic_head = _split_topic(topic)
        root_node, root_name = self._get_root(topic_head)

        # workaround to make the tree node matchable
        topic_parts.insert(0, root_name)
        root_node.add_subscription(subscription, topic_parts)

    def remove_subscription(self, clientid, topic):
        _validate_subscription_mask(topic)

        topic_parts, topic_head = _split_topic(topic)
        root_node, root_name = self._get_root(topic_head)

        # workaround to make the tree node matchable
        topic_parts.insert(0, root_name)
        root_node.remove_subscription(clientid, topic_parts)

    def remove_client(self, clientid):
        self.sys_root_node.remove_client(clientid)
        self.root_node.remove_client(clientid)

    def get_subscriptions(self, topic):
        if SubscriptionWildcards.MULTI_LEVEL in topic or \
                SubscriptionWildcards.SINGLE_LEVEL in topic:
            raise SubscriptionError('[MQTT-4.7.1-1] Topic names should not contain wildcards')

        topic_parts, topic_head = _split_topic(topic)
        root_node, root_name = self._get_root(topic_head)

        yield from root_node.get_subscriptions(topic_parts)

    def get_client_subscriptions(self, clientid, topic):
        return [s for s in self.get_subscriptions(topic) if s.clientid == clientid]

    def _get_root(self, topic_head):
        # str topic_head: the first topic part
        if topic_head and topic_head[0] == self.SYS_SPECIAL_CHAR:
            return self.sys_root_node, 'sys_root'
        else:
            return self.root_node, 'std_root'

    def __str__(self):
        lines = list()
        lines.append('[SYS_ROOT]')
        lines.append(str(self.sys_root_node))
        lines.append('[ROOT]')
        lines.append(str(self.root_node))
        return '\n'.join(lines)


class SubscriptionNode:
    __slots__ = ['name', 'children', 'subscribers', 'multilevel_subscribers']

    def __init__(self):
        self.children = dict()

        # the following is a list of `Subscription`
        self.subscribers = set()
        self.multilevel_subscribers = set()

    def add_subscription(self, subscription, topic_parts):
        head, tail = topic_parts[0], topic_parts[1:]

        assert isinstance(head, str)

        if not tail:
            # reached the last topic part

            # remove duplicate subscriptions
            self._remove_client(subscription.clientid)

            # add new subscription
            self.subscribers.add(subscription)

        elif tail[0] == SubscriptionWildcards.MULTI_LEVEL:
            # remove duplicate subscriptions
            self._remove_client_multilevel(subscription.clientid)

            # add new subscription
            self.multilevel_subscribers.add(subscription)

        else:
            self._propagate_add(subscription, tail)

    def _propagate_add(self, subscription, subscription_tail):
        next_part = subscription_tail[0]
        assert isinstance(next_part, str)

        try:
            child = self.children[next_part]

        except KeyError:
            self.children[next_part] = child = SubscriptionNode()

        child.add_subscription(subscription, subscription_tail)

    def remove_subscription(self, clientid, topic_parts):
        head, tail = topic_parts[0], topic_parts[1:]

        assert isinstance(head, str)

        if not tail:
            self._remove_client(clientid)

        elif tail[0] == SubscriptionWildcards.MULTI_LEVEL:
            self._remove_client_multilevel(clientid)

        else:
            self._propagate_remove(clientid, tail)

    def remove_client(self, clientid):
        self._remove_client(clientid)
        self._remove_client_multilevel(clientid)

        children_to_remove = list()

        for name, child in self.children.items():
            child.remove_client(clientid)

            if child.is_empty:
                children_to_remove.append(name)

        for name in children_to_remove:
            del self.children[name]

    def _remove_client(self, clientid):
        for subscription in list(self.subscribers):
            if subscription.clientid == clientid:
                self.subscribers.remove(subscription)

    def _remove_client_multilevel(self, clientid):
        for subscription in list(self.multilevel_subscribers):
            if subscription.clientid == clientid:
                self.multilevel_subscribers.remove(subscription)

    def _propagate_remove(self, clientid, subscription_tail):
        name = subscription_tail[0]

        try:
            child = self.children[name]
            assert isinstance(child, SubscriptionNode)

            child.remove_subscription(clientid, subscription_tail)

            if child.is_empty:
                del self.children[name]

        except KeyError:
            pass

    def get_subscriptions(self, topic_parts):
        if topic_parts:
            assert isinstance(topic_parts[0], str)

            tail = topic_parts[1:]
            yield from self.multilevel_subscribers
            yield from self._child_subscribers(topic_parts[0], tail)
            yield from self._child_subscribers(SubscriptionWildcards.SINGLE_LEVEL, tail)

        else:
            yield from self.multilevel_subscribers
            yield from self.subscribers

    def _child_subscribers(self, topic_part, tail):
        try:
            return self.children[topic_part].get_subscriptions(tail)

        except KeyError:
            return list()

    @property
    def is_empty(self):
        return len(self.subscribers) == 0 and len(self.multilevel_subscribers) == 0 and len(self.children) == 0

    def __str__(self):
        lines = list()

        if self.subscribers:
            lines.append(u'  - subscribers ({})'.format(len(self.subscribers)))
            lines.extend([u'      {}'.format(subscriber) for subscriber in self.subscribers])

        if self.multilevel_subscribers:
            lines.append(u'  - multilevel_subs ({})'.format(len(self.multilevel_subscribers)))
            lines.extend([u'      {}'.format(subscriber) for subscriber in self.multilevel_subscribers])

        for name, child in self.children.items():
            lines.append(u'  {}/'.format(name))
            lines.extend([u'  ' + line for line in str(child).splitlines()])

        return '\n'.join(lines)


Subscription = namedtuple('Subscription', ['clientid', 'topic', 'qos'])


def _split_topic(topic):
    if not topic:
        raise SubscriptionError('Topic Names and Topic Filters MUST be at least one character long [MQTT-4.7.3-1]')

    topic_parts = topic.split(SubscriptionTree.TOPIC_SEPARATOR)

    if not topic_parts:
        raise SubscriptionError('Topic Names and Topic Filters MUST be at least one character long [MQTT-4.7.3-1]')

    return topic_parts, topic_parts[0]


def match(mask, topic):
    """
    Checks if topic matches the given mask.
    """
    if SubscriptionWildcards.MULTI_LEVEL in topic or \
            SubscriptionWildcards.SINGLE_LEVEL in topic:
        raise SubscriptionError('[MQTT-4.7.1-1] Topic names should not contain wildcards')

    _validate_subscription_mask(mask)

    mask_parts, _ = _split_topic(mask)
    topic_parts, _ = _split_topic(topic)

    for mask_part, topic_part in zip_longest(mask_parts, topic_parts):
        if mask_part == SubscriptionWildcards.MULTI_LEVEL:
            return True

        elif mask_part is None or topic_part is None:
            return False

        elif mask_part != SubscriptionWildcards.SINGLE_LEVEL \
                and mask_part != topic_part:
            return False

    return True


def _validate_subscription_mask(mask):
    if "\x00" in mask:
        raise SubscriptionError('Subscription mask should not contain NUL char')

    parts = mask.split(SubscriptionTree.TOPIC_SEPARATOR)
    if len(parts) == 0:
        return True

    elif len(parts) == 1:
        _validate_mask_level(parts[0], is_last=True)

    else:
        body = parts[:-1]
        last = parts[-1]

        for s in body:
            _validate_mask_level(s, is_last=False)

        _validate_mask_level(last, is_last=True)


def _validate_mask_level(s, is_last):
    if s == SubscriptionWildcards.SINGLE_LEVEL:
        return True

    elif s == SubscriptionWildcards.MULTI_LEVEL:
        if is_last:
            return True

        else:
            raise SubscriptionError('Unexpected multi-level wildcard')

    elif SubscriptionWildcards.SINGLE_LEVEL in s:
        raise SubscriptionError('Incorrect single-level wildcard usage')

    elif SubscriptionWildcards.MULTI_LEVEL in s:
        raise SubscriptionError('Incorrect multi-level wildcard usage')

    else:
        return True
