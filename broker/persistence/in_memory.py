from datetime import datetime
from collections import deque, OrderedDict

from broker.persistence import PersistenceBase, ClientPersistenceBase, OutgoingPublishesBase, OutgoingEntry


class InMemoryPersistence(PersistenceBase):
    def __init__(self):
        self.retained_messages = dict()
        self.clients = dict()

    def get_client_uids(self):
        return self.clients.keys()

    def get_retained_messages(self):
        return self.retained_messages

    def get_for_client(self, uid):
        client_persistence = self.clients.get(uid, None)

        if client_persistence is None:
            client_persistence = InMemoryClientPersistence(uid)
            self.clients[uid] = client_persistence

        return client_persistence

    def remove_client_data(self, uid):
        self.clients.pop(uid, None)


class InMemoryClientPersistence(ClientPersistenceBase):
    def __init__(self, uid):
        super().__init__(uid)
        self._subscriptions = dict()
        self._incoming_packet_ids = set()
        self._outgoing_publishes = InMemoryOutgoingPublishes()

        self.last_seen = datetime.now()

    @property
    def subscriptions(self):
        return self._subscriptions

    @property
    def incoming_packet_ids(self):
        return self._incoming_packet_ids

    @property
    def outgoing_publishes(self):
        return self._outgoing_publishes


class InMemoryOutgoingPublishes(OutgoingPublishesBase):
    def __init__(self):
        self._queue = deque()
        self._inflight = OrderedDict()

    def insert(self, msg):
        self._queue.append(msg)

    def queue_len(self):
        return len(self._queue)

    def peek(self, i):
        if i < len(self._queue):
            return self._queue[i]

        else:
            return None

    def remove(self, n=1):
        for i in range(n):
            self._queue.popleft()

    def register_inflight(self, msg):
        self._inflight[msg.id] = OutgoingEntry(msg.id, msg.copy())

    @property
    def inflight_len(self):
        return len(self._inflight)

    def is_inflight(self, packet_id):
        return packet_id in self._inflight

    def get_all_inflight(self):
        for packet_id, entry in self._inflight.items():
            yield entry.message.copy()

    def inflight_packet_ids(self):
        return self._inflight.keys()

    def set_sent(self, packet_id):
        self._inflight[packet_id].sent = True

    def is_sent(self, packet_id):
        return self._inflight[packet_id].sent

    def set_pubconf(self, packet_id):
        self._inflight[packet_id].conf = True

    def is_pubconf(self, packet_id):
        return self._inflight[packet_id].conf

    def remove_inflight(self, packet_id):
        self._inflight.pop(packet_id, None)

    def clear(self):
        self._queue.clear()
        self._inflight.clear()
