from collections import UserDict
from redis import StrictRedis


class RedisHashDict(UserDict):
    def __init__(self, redis, hash_name):
        assert isinstance(redis, StrictRedis)
        super().__init__()
        self.redis = redis
        self.name = hash_name

    def __contains__(self, key):
        return self.redis.hexists(self.name, key)

    def __len__(self):
        return self.redis.hlen(self.name)

    def __getitem__(self, key):
        value = self.redis.hget(self.name, key)

        if value is not None:
            return self._decode_val(value)

        raise KeyError(key)

    def __setitem__(self, key, value):
        return self.redis.hset(self.name, key, value)

    def __delitem__(self, key):
        return self.redis.hdel(self.name, key)

    def items(self):
        for k, v in self.redis.hscan_iter(self.name):
            yield (self._decode_key(k), self._decode_val(v))

    def keys(self):
        yield from (self._decode_key(k) for k in self.redis.hkeys(self.name))

    def values(self):
        yield from (self._decode_val(v) for v in self.redis.hvals(self.name))

    def _decode_key(self, key):
        # override to decode keys retrieved from redis
        return key

    def _decode_val(self, val):
        # override to decode values retrieved from redis
        return val

    def clear(self):
        self.redis.delete(self.name)


class RedisList():
    def __init__(self, redis, key):
        self.redis = redis
        self.key = key

    def append(self, value):
        self.redis.rpush(self.key, value)

    def remove(self, value):
        self.redis.lrem(self.key, 1, value)

    def items(self):
        for x in self.redis.lscan_iter(self.key):
            yield self._decode(x)

    def pop(self):
        return self._decode(self.redis.lpop(self.key))

    def __getitem__(self, k):
        return self.redis.lindex(self.key, k)

    def __iter__(self):
        for val in self.redis.lrange(self.key, 0, -1):
            yield self._decode(val)

    def __contains__(self, item):
        # preferable to not use this
        for x in self:
            if self._decode(x) == item:
                return True

        return False

    def __len__(self):
        return self.redis.llen(self.key)

    def _decode(self, val):
        # override to decode items retrieved from redis
        return val

    def clear(self):
        self.redis.delete(self.key)


class RedisIntList(RedisList):
    def _decode(self, val):
        return int(val)


class RedisSet():
    def __init__(self, redis, key):
        self.redis = redis
        self.key = key

    def add(self, value):
        self.redis.sadd(self.key, value)

    def remove(self, value):
        if value in self:
            self.redis.srem(self.key, value)
        else:
            raise KeyError(value)

    def discard(self, value):
        self.redis.srem(self.key, value)

    def items(self):
        for x in self.redis.sscan_iter(self.key):
            yield self._decode(x)

    def __iter__(self):
        return iter(self.items())

    def __len__(self):
        return self.redis.scard(self.key)

    def __contains__(self, item):
        return self.redis.sismember(self.key, item)

    def _decode(self, val):
        # override to decode items retrieved from redis
        return val

    def clear(self):
        self.redis.delete(self.key)


class RedisUnicodeSet(RedisSet):
    def _decode(self, val):
        return val.decode("utf-8") if val else val


class RedisIntSet(RedisSet):
    def _decode(self, val):
        return int(val) if val else val
