import logging
from datetime import datetime
from backports.datetime_fromisoformat import MonkeyPatch as FromIsoFormatMp

from broker.factory import MQTTMessageFactory
from broker.persistence import PersistenceBase, ClientPersistenceBase, OutgoingPublishesBase
from .redis_types import RedisHashDict, RedisIntSet, RedisUnicodeSet, RedisIntList, RedisList


if not hasattr(datetime, 'fromisoformat'):
    FromIsoFormatMp.patch_fromisoformat()

logger = logging.getLogger('persistence.redis')


class RedisPersistence(PersistenceBase):
    def __init__(self, redis):
        self.redis = redis
        self.client_uids = RedisUnicodeSet(redis, key="mqtt_broker:client_uids")

    def get_client_uids(self):
        return self.client_uids

    def get_retained_messages(self):
        return RedisRetainedMessagesDict(self.redis, '_retained_messages')

    def get_for_client(self, uid):
        self.client_uids.add(uid)
        return RedisClientPersistence(self.redis, uid)

    def remove_client_data(self, uid):
        if uid in self.client_uids:
            self.client_uids.remove(uid)
            RedisClientPersistence(self.redis, uid).clear()


class RedisClientPersistence(ClientPersistenceBase):
    def __init__(self, redis, uid):
        super().__init__(uid)
        self.redis = redis

        self._subscriptions = RedisSubscriptionsDict(
            hash_name="%s:subscriptions" % uid,
            redis=self.redis)

        self._incoming_packet_ids = RedisIntSet(redis=self.redis,
                                                key="%s:incoming_packet_ids" % uid)
        self._outgoing_publishes = RedisOutgoingPublishes(redis=self.redis, uid=uid)

        self._last_seen_key = '{}:last_seen'.format(self.uid)

        if self.last_seen is None:
            # ensures last_seen is never None
            self.last_seen = datetime.now()

    @property
    def subscriptions(self):
        return self._subscriptions

    @property
    def incoming_packet_ids(self):
        return self._incoming_packet_ids

    @property
    def outgoing_publishes(self):
        return self._outgoing_publishes

    def clear(self):
        self._subscriptions.clear()
        self._incoming_packet_ids.clear()
        self._outgoing_publishes.clear()
        self.redis.delete(self._last_seen_key)

    @property
    def last_seen(self):
        value = self.redis.get(self._last_seen_key)
        if value:
            return datetime.fromisoformat(value.decode("utf-8"))

    @last_seen.setter
    def last_seen(self, value):
        if isinstance(value, datetime):
            self.redis.set(self._last_seen_key, value.isoformat())

        else:
            self.redis.delete(self._last_seen_key)


class RedisSubscriptionsDict(RedisHashDict):
    @staticmethod
    def _decode_key(k):
        return k.decode("utf-8") if k else k

    @staticmethod
    def _decode_val(v):
        return int(v) if v else v


class RedisOutgoingPublishes(OutgoingPublishesBase):
    def __init__(self, redis, uid):
        self.redis = redis
        self.uid = uid

        self._queue = RedisList(redis, "%s:outgoing_queue" % uid)
        self._inflight = RedisPacketsDict(redis, "%s:outgoing_inflight" % uid)

        self._inflight_ids = RedisIntList(redis, '%s:outgoing_ids' % uid)
        self._sent_ids = RedisIntSet(redis, "%s:outgoing_sent_ids" % uid)
        self._confirmed_ids = RedisIntSet(redis, "%s:outgoing_conf_ids" % uid)

    def insert(self, msg):
        msg.id = 0  # sets a dummy id just to make it encode,
                    # the actual id is set during delivery.
        self._queue.append(msg.raw_data)

    def queue_len(self):
        return len(self._queue)

    def peek(self, i=0):
        """
        Gets the next packet to be published.
        Should be used to start a new flow.
        """
        if i < len(self._queue):
            raw_data = self._queue[i]
            return MQTTMessageFactory.make(raw_data)

        else:
            return None

    def remove(self, n=1):
        for i in range(n):
            self._queue.pop()

    def register_inflight(self, msg):
        self._inflight_ids.append(msg.id)
        self._inflight[msg.id] = msg.raw_data

    @property
    def inflight_len(self):
        return len(self._inflight)

    def is_inflight(self, packet_id):
        return packet_id in self._inflight

    def get_all_inflight(self):
        for packet_id in self._inflight_ids:
            raw = self._inflight.get(packet_id)
            if raw is not None:
                yield MQTTMessageFactory.make(raw)

    def inflight_packet_ids(self):
        for packet_id in self._inflight_ids:
            yield packet_id

    def set_sent(self, packet_id):
        if packet_id in self._inflight:
            self._sent_ids.add(packet_id)
        else:
            raise KeyError('Unknown packet id')

    def is_sent(self, packet_id):
        if packet_id in self._inflight:
            return packet_id in self._sent_ids
        else:
            raise KeyError('Unknown packet id')

    def set_pubconf(self, packet_id):
        if packet_id in self._inflight:
            self._confirmed_ids.add(packet_id)
        else:
            raise KeyError('Unknown packet id')

    def is_pubconf(self, packet_id):
        if packet_id in self._inflight:
            return packet_id in self._confirmed_ids
        else:
            raise KeyError('Unknown packet id')

    def remove_inflight(self, packet_id):
        # todo: do in a transaction
        if packet_id in self._inflight:
            self._inflight_ids.remove(packet_id)
            self._sent_ids.discard(packet_id)
            self._confirmed_ids.discard(packet_id)
            del self._inflight[packet_id]

    def clear(self):
        self.redis.delete(
            '%s:outgoing_queue' % self.uid,
            '%s:outgoing_inflight' % self.uid,
            '%s:outgoing_ids' % self.uid,
            '%s:outgoing_sent_ids' % self.uid,
            "%s:outgoing_conf_ids" % self.uid,
            )


class RedisPacketsDict(RedisHashDict):
    @staticmethod
    def _decode_key(k):
        return int(k) if k else k


class RedisRetainedMessagesDict(RedisHashDict):
    @staticmethod
    def _decode_key(key):
        return str(key, encoding="UTF-8") if key else key
