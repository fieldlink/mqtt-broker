
class PersistenceBase():
    """
    Base 'interface' class that defines the basic methods for broker
    persistence.
    """
    def get_client_uids(self):
        """
        Retrieves the persisted clients uids.
        :return: an iterable list of uids (strings)
        """
        pass

    def get_retained_messages(self):
        """
        Retrieves the retained publishes
        :return: a dict[topic, publish]
        """
        pass

    def get_for_client(self, uid):
        """
        Get or creates an object to access persistence
        for a client.
        :return: a subclass of ClientPersistenceBase
        """
        return ClientPersistenceBase(uid)

    def remove_client_data(self, uid):
        """
        Permanently removes client persistence data
        """
        pass


class ClientPersistenceBase():
    def __init__(self, uid):
        self.uid = uid

        # Subclassses should implement the following property
        # self.last_seen = datetime.now()

    @property
    def subscriptions(self):
        """
        Retrieves the topics (and qos's) subscribed by the client.
        :return: a dict[topic, qos]
        """
        return None

    @property
    def incoming_packet_ids(self):
        """
        Retrieves the set of packet ids being received.
        Only used for QoS level 2 incoming packets
        :return: set of int
        """
        return None

    @property
    def outgoing_publishes(self):
        """
        Retrieves th
        :return:
        """
        return None


class OutgoingPublishesBase():
    """
    This class defines the protocol of the OutgoingPublishes persistence
    classes. Persistence should support storing an arbitrary number of
    messages, not being limited by the packet id limit (65K).

    The implementing class must store the messages in a queue when
    `insert` is called. Then, when a new publish flow is being started,
    `get_next` method is called. The `get_next` method should pop an item
    from that queue, assign a free packet id and persist it in an in-flight
    structure. Finnaly, when the flow is completed the `remove` method is
    called.

    Other methods are required to retrieve in-flight items and state, and
    to get and set 'sent' and 'confirmed' flags.
    """

    def insert(self, msg):
        """
        Creates and pushes a new outgoing publish entry, to the end of a
        queue.
        """
        raise NotImplementedError

    def queue_len(self):
        """
        Gets the size of the outgoing queue. In other words, the number
        of messages that can be fetched using `get_next` method.
        :return: int
        """
        raise NotImplementedError

    def peek(self, i):
        """
        Gets the next packet to be published, without removing it from the
        queue.

        Returns the publish packet when succeeded, and None when the queue is
        empty.
        :param i: index of the queue to peek
        """
        raise NotImplementedError

    def remove(self, n=1):
        """
        Gets and returns n entries from the beginning of the queue.
        :param n: the quantity of entries to pop
        """
        raise NotImplementedError

    # todo: create new classes for inflight messages
    def register_inflight(self, msg):
        """
        Registers the message on the in-flight structure.
        """
        raise NotImplementedError

    @property
    def inflight_len(self):
        """
        Gets the count of publishes in-flight.
        """
        raise NotImplementedError

    def is_inflight(self, packet_id):
        """
        Checks if the given packet_id is still in in-flight state.
        """
        raise NotImplementedError

    def get_all_inflight(self):
        """
        Gets all the in-flight packets in the *same order* provided by the
        `get_next` method.

        This method is called when retrying to publish all the in-flight
        messages.
        """
        raise NotImplementedError

    def inflight_packet_ids(self):
        """
        Gets all in-flight packets ids.
        Not necessarily in the order of delivery.
        :return: 
        """
        raise NotImplementedError

    def set_sent(self, packet_id):
        """
        Marks an outgoing publish with "publish sent".
        This is used with publishes in QoS levels 1 or 2, after a publish
        packet is successfully written.
        """
        raise NotImplementedError

    def is_sent(self, packet_id):
        """
        Checks whether the publish was previously sent or not.
        This is used with publishes in QoS levels 1 or 2 to decide to set the
        DUP flag or not.
        """
        raise NotImplementedError

    def set_pubconf(self, packet_id):
        """
        Marks an outgoing publish with "publish confirmed".
        This is used with publishes in QoS level 2 exclusively, after a PUBREC
        packet is received.
        """
        raise NotImplementedError

    def is_pubconf(self, packet_id):
        """
        Checks whether the publish was confirmed or not.
        This is used with publishes in QoS level 2 exclusively, when deciding
        to send a PUBLISH packet or a PUBREL packet.
        """
        raise NotImplementedError

    def remove_inflight(self, packet_id):
        """
        Removes a publish entry from the in-flight structure by packet_id.
        After this method finishes, the packet_id can be reused.
        """
        raise NotImplementedError

    def clear(self):
        """
        Clears all the data in the outgoing queue.
        """
        raise NotImplementedError


class OutgoingEntry:
    __slots__ = ('packet_id', 'message', 'sent', 'conf')

    def __init__(self, packet_id=None, message=None, sent=False, conf=False):
        self.packet_id = packet_id
        self.message = message
        self.sent = sent
        self.conf = conf


from .in_memory import InMemoryPersistence, InMemoryClientPersistence
from .redis import RedisPersistence
