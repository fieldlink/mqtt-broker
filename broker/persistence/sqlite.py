# -*- coding:utf-8 -*-
import sqlite3

from contextlib import closing

from tornado.ioloop import PeriodicCallback

from broker.factory import MQTTMessageFactory
from broker.persistence import PersistenceBase, ClientPersistenceBase, OutgoingPublishesBase, PacketIdGenerator


class SqlitePersistence(PersistenceBase):
    _commit_frequency = 20000  # in ms
    _commit_quantity = 1000
    def __init__(self, location):
        self.connection = sqlite3.connect(location, isolation_level="EXCLUSIVE")

        # clients table
        self.connection.execute('CREATE TABLE IF NOT EXISTS clients (id INTEGER PRIMARY KEY, uid TEXT)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS clients_id_idx ON clients (id)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS clients_uid_idx ON clients (uid)')

        # subscriptions table
        self.connection.execute('CREATE TABLE IF NOT EXISTS subscriptions '
                                '(id INTEGER PRIMARY KEY, client_id INTEGER, topic TEXT, qos INTEGER)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS subscriptions_client_id_idx '
                                'ON subscriptions (client_id)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS subscriptions_topic_idx '
                                'ON subscriptions (topic)')

        # incoming messages table
        self.connection.execute('CREATE TABLE IF NOT EXISTS incoming_messages '
                                '(id INTEGER PRIMARY KEY, client_id INTEGER, packet_id INTEGER)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS incoming_messages_client_id_idx '
                                'ON incoming_messages (client_id)')

        # outgoing messages table
        self.connection.execute('CREATE TABLE IF NOT EXISTS outgoing_messages '
                                '(id INTEGER PRIMARY KEY, client_id INTEGER, message BLOB, '
                                'packet_id INTEGER, is_sent BOOLEAN, is_pubconf BOOLEAN)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS outgoing_messages_client_id_idx '
                                'ON outgoing_messages (client_id)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS outgoing_messages_packet_id_idx '
                                'ON outgoing_messages (packet_id)')

        # retained messages table
        self.connection.execute('CREATE TABLE IF NOT EXISTS retained_messages '
                                '(topic TEXT, message BLOB)')
        self.connection.execute('CREATE INDEX IF NOT EXISTS retained_messages_topic_idx '
                                'ON retained_messages (topic)')

        self.connection.commit()
        self._retained_messages = SqliteRetainedMessages(self.connection)

        # committer follows redis persistence idea
        self._next_commit_milestone = 1000
        self.committer = PeriodicCallback(self._check_commit, self._commit_frequency)
        self.committer.start()

    def _check_commit(self):
        if self.connection.total_changes > self._next_commit_milestone:
            print('committing sqlite data')
            self.connection.commit()
            n = self._commit_quantity
            self._next_commit_milestone = n + (self.connection.total_changes / n) * n

    def get_client_uids(self):
        res = self.connection.execute("SELECT uid FROM clients")
        return [r[0] for r in res]

    def get_retained_messages(self):
        return self._retained_messages

    def get_for_client(self, uid):
        return SqliteClientPersistence(uid, self.connection)

    def remove_client_data(self, uid):
        if self._client_is_known(uid):
            self.get_for_client(uid).clear()

    def _client_is_known(self, uid):
        c = self.connection.execute("SELECT 1 FROM clients WHERE uid=? LIMIT 1", (uid, ))
        return c.fetchone() is not None

    def close(self):
        self.connection.commit()
        self.connection.close()


class SqliteClientPersistence(ClientPersistenceBase):
    def __init__(self, uid, conn):
        super().__init__(uid)
        self.connection = conn

        c = self.connection.execute('SELECT id FROM clients WHERE uid=? LIMIT 1', (uid, ))
        row = c.fetchone()
        if row is not None:
            self.client_id = row[0]
        else:
            c = self.connection.execute('INSERT INTO clients (uid) VALUES (?)', (uid, ))
            self.client_id = c.lastrowid
            # self.connection.commit()

        self._subscriptions = SqliteSubscriptions(self.client_id, conn)
        self._incoming_packet_ids = SqliteIncomingMessages(self.client_id, conn)
        self._outgoing_publishes = SqliteOutgoingPublishes(self.client_id, conn)

    @property
    def subscriptions(self):
        return self._subscriptions

    @property
    def incoming_packet_ids(self):
        return self._incoming_packet_ids

    @property
    def outgoing_publishes(self):
        return self._outgoing_publishes

    def clear(self):
        self.connection.execute("DELETE FROM clients WHERE id=?", (self.client_id, ))

        self._subscriptions.clear()
        self._incoming_packet_ids.clear()
        self._outgoing_publishes.clear()

        # self.connection.commit()


class SqliteSubscriptions:
    def __init__(self, client_id, connection):
        self.connection = connection
        self.client_id = client_id

    def get(self, topic, default=None):
        return self._get(topic, default)

    def _get(self, topic, default):
        c = self.connection.execute('SELECT qos FROM subscriptions '
                                    'WHERE client_id=? AND topic=?',
                                    (self.client_id, topic))
        row = c.fetchone()
        if row:
            return row[0]
        else:
            return default

    def pop(self, topic, default=None):
        qos = self._get(topic, default)
        if qos is not None:
            self.connection.execute('DELETE FROM subscriptions '
                                    'WHERE client_id=? AND topic=?',
                                    (self.client_id, topic))
            # self.connection.commit()

        return qos

    def __len__(self):
        c = self.connection.execute('SELECT COUNT(*) FROM subscriptions '
                                    'WHERE client_id=?',
                                    (self.client_id, ))
        return c.fetchone()[0]

    def items(self):
        c = self.connection.execute('SELECT topic, qos FROM subscriptions '
                                    'WHERE client_id=?',
                                    (self.client_id, ))
        return c.fetchall()

    def keys(self):
        c = self.connection.execute('SELECT topic FROM subscriptions WHERE client_id=?',
                                    (self.client_id, ))
        return [r[0] for r in c.fetchall()]

    def __setitem__(self, topic, qos):
        if self._contains(topic):
            cmd = 'UPDATE subscriptions SET qos=? WHERE client_id=? AND topic=?'
        else:
            cmd = 'INSERT INTO subscriptions (qos, client_id, topic) VALUES (?, ?, ?)'

        self.connection.execute(cmd, (qos, self.client_id, topic))
        # self.connection.commit()

    def __getitem__(self, topic):
        qos = self.get(topic, None)

        if qos is None:
            raise KeyError('subscription not found for (%s, %s)', self.client_id, topic)

        return qos

    def __contains__(self, topic):
        return self._contains(topic)

    def _contains(self, topic):
        c = self.connection.execute('SELECT 1 FROM subscriptions '
                                    'WHERE client_id=? AND topic=? '
                                    'LIMIT 1',
                                    (self.client_id, topic))
        return c.fetchone() is not None

    def clear(self):
        self.connection.execute("DELETE FROM subscriptions WHERE client_id=?", (self.client_id, ))


class SqliteIncomingMessages:
    def __init__(self, client_id, conn):
        self.client_id = client_id
        self.connection = conn

    def add(self, packet_id):
        if not self._contains(packet_id):
            self.connection.execute('INSERT INTO incoming_messages (client_id, packet_id) '
                                    'VALUES (?, ?)', (self.client_id, packet_id))

        # self.connection.commit()

    def remove(self, packet_id):
        c = self.connection.execute('DELETE FROM incoming_messages '
                                    'WHERE client_id=? AND packet_id=?',
                                    (self.client_id, packet_id))
        if c.rowcount == 0:
            raise KeyError(packet_id)

        # self.connection.commit()

    def discard(self, packet_id):
        self.connection.execute('DELETE FROM incoming_messages '
                                'WHERE client_id=? AND packet_id=?',
                                (self.client_id, packet_id))
        # self.connection.commit()

    def __contains__(self, packet_id):
        return self._contains(packet_id)

    def _contains(self, packet_id):
        cursor = self.connection.execute('SELECT 1 FROM incoming_messages '
                                         'WHERE client_id=? AND packet_id=? '
                                         'LIMIT 1',
                                         (self.client_id, packet_id))
        return cursor.fetchone() is not None

    def clear(self):
        self.connection.execute("DELETE FROM incoming_messages WHERE client_id=?", (self.client_id, ))


class SqliteOutgoingPublishes(OutgoingPublishesBase):
    def __init__(self, client_id, conn):
        self.client_id = client_id
        self.connection = conn

        ids = SqliteOutgoingInflightIds(client_id, conn)
        self._ids_gen = PacketIdGenerator(ids)

    def insert(self, msg):
        msg.id = 0  # sets a dummy id just to make it encode,
                    # the actual id is set on `get_next` method.

        self.connection.execute('INSERT INTO outgoing_messages '
                                '(client_id, message, is_sent, is_pubconf) '
                                'VALUES (?, ?, 0, 0)',
                                (self.client_id, msg.raw_data))

        # self.connection.commit()

    def peek(self):
        c = self.connection.execute('SELECT id, message FROM outgoing_messages '
                      'WHERE client_id=? AND packet_id IS NULL '
                      'ORDER BY id ASC '
                      'LIMIT 1',
                      (self.client_id,))
        row = c.fetchone()
        if row is not None:
            row_id, msg = row
            msg = MQTTMessageFactory.make(msg)
            msg.id = self._ids_gen.next()

            self._set_inflight(row_id, msg)

            # self.connection.commit()
            return msg.copy()
        else:
            return None

    def _set_inflight(self, row_id, message):
        self.connection.execute('UPDATE outgoing_messages SET message=?, packet_id=? '
                                'WHERE id=? AND client_id=?',
                                (message.raw_data, message.id, row_id, self.client_id))

    @property
    def inflight_len(self):
        return self._inflight_len()

    def _inflight_len(self):
        c = self.connection.execute('SELECT COUNT(*) FROM outgoing_messages '
                                    'WHERE client_id=? AND packet_id IS NOT NULL',
                                    (self.client_id,))
        return c.fetchone()[0]

    def is_inflight(self, packet_id):
        c = self.connection.execute('SELECT 1 FROM outgoing_messages '
                                    'WHERE client_id=? AND packet_id=?',
                                    (self.client_id, packet_id))
        return c.fetchone() is not None

    def get_all_inflight(self):
        with closing(self.connection.cursor()) as c:
            c.execute('SELECT message FROM outgoing_messages '
                      'WHERE client_id=? AND packet_id IS NOT NULL '
                      'ORDER BY id ASC',
                      (self.client_id,))
            return [MQTTMessageFactory.make(r[0]) for r in c.fetchall()]

    def set_sent(self, packet_id):
        c = self.connection.execute('UPDATE outgoing_messages SET is_sent=1 '
                                    'WHERE client_id=? AND packet_id=?',
                                    (self.client_id, packet_id))
        if c.rowcount == 0:
            raise KeyError(packet_id)

        # self.connection.commit()

    def is_sent(self, packet_id):
        c = self.connection.execute('SELECT is_sent FROM outgoing_messages '
                                    'WHERE client_id=? AND packet_id=?',
                                    (self.client_id, packet_id))
        row = c.fetchone()
        if row is not None:
            return row[0]
        else:
            raise KeyError(packet_id)

    def set_pubconf(self, packet_id):
        c = self.connection.execute('UPDATE outgoing_messages SET is_pubconf=1 '
                                    'WHERE client_id=? AND packet_id=?',
                                    (self.client_id, packet_id))
        if c.rowcount == 0:
            raise KeyError(packet_id)

        # self.connection.commit()

    def is_pubconf(self, packet_id):
        c = self.connection.execute('SELECT is_pubconf FROM outgoing_messages '
                                    'WHERE client_id=? AND packet_id=?',
                                    (self.client_id, packet_id))
        row = c.fetchone()
        if row is not None:
            return row[0]
        else:
            raise KeyError(packet_id)

    def remove(self, packet_id):
        c = self.connection.execute('DELETE FROM outgoing_messages '
                                    'WHERE client_id=? AND packet_id=?',
                                    (self.client_id, packet_id))

        # self.connection.commit()

    def clear(self):
        self.connection.execute("DELETE FROM outgoing_messages WHERE client_id=?", (self.client_id, ))


class SqliteOutgoingInflightIds:
    def __init__(self, client_id, conn):
        self.client_id = client_id
        self.connection = conn

    def __len__(self):
        c = self.connection.execute('SELECT COUNT(*) FROM outgoing_messages WHERE client_id=?',
                                    (self.client_id, ))
        return c.fetchone()[0]

    def __contains__(self, packet_id):
        c = self.connection.execute('SELECT 1 FROM outgoing_messages '
                                    'WHERE client_id=? AND packet_id=? '
                                    'LIMIT 1',
                                    (self.client_id, packet_id))
        return c.fetchone() is not None


class SqliteRetainedMessages:
    def __init__(self, conn):
        self.connection = conn

    def __contains__(self, topic):
        c = self.connection.execute('SELECT 1 FROM retained_messages WHERE topic=? LIMIT 1', (topic,))
        return c.fetchone() is not None

    def __delitem__(self, topic):
        self.connection.execute('DELETE FROM retained_messages WHERE topic=? LIMIT 1', (topic,))
        # self.connection.commit()

    def __setitem__(self, topic, message):
        if topic in self:
            cmd = 'UPDATE retained_messages SET message=? WHERE topic=?'
        else:
            cmd = 'INSERT INTO retained_messages (message, topic) VALUES (?, ?)'

        self.connection.execute(cmd, (message, topic))
        # self.connection.commit()

    def items(self):
        c = self.connection.execute('SELECT topic, message FROM retained_messages')
        return [r for r in c.fetchall()]
