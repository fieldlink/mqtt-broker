
class ConnectError(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.message = message


class SubscriptionError(Exception):
    def __init__(self, message):
        self.message = message
