import logging

from datetime import datetime, timedelta
from tornado.ioloop import PeriodicCallback


STALED_SESSIONS_PURGE_PERIOD = timedelta(hours=6)
MAX_SESSION_AGE = timedelta(days=90)
MAX_PENDING_MESSAGES = 100000


logger = logging.getLogger('broker.tasks')


def start_purge_staled_sessions_task(clients, servers):
    # calls function to run immediately, because the PeriodicCallback will
    # run the function the first time just after the period elapses
    purge_staled_sessions(clients, servers)

    task = PeriodicCallback(lambda: purge_staled_sessions(clients, servers),
                            STALED_SESSIONS_PURGE_PERIOD.total_seconds() * 1000)

    task.start()
    return task


def purge_staled_sessions(clients, servers):
    try:
        logger.info('purging staled sessions')
        now = datetime.now()

        for client in list(clients.values()):
            last_seen = client.last_seen
            if last_seen is None:
                logger.warning('[uid:{}] last seen date is empty'.format(client.uid))

            elif client.is_connected():
                logger.debug('[uid:{}] updating client last seen time'.format(client.uid))
                client.last_seen = datetime.now()

            elif now - last_seen > MAX_SESSION_AGE \
                    or client.pending_messages > MAX_PENDING_MESSAGES:
                logger.debug('[uid:{}] purging client session'.format(client.uid))
                for server in servers:
                    server.remove_client(client)

    except Exception:
        logger.error('could not purge staled sessions', exc_info=True)
