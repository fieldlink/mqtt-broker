from logging import getLogger
from tornado import gen
from tornado import websocket


logger = getLogger('activity.websockets')

MQTT_SUBPROTOCOL = 'mqtt'


class MqttWebSocket(websocket.WebSocketHandler):
    target_server = None

    def __init__(self, *args, **kwargs):
        super(MqttWebSocket, self).__init__(*args, **kwargs)

        self._subprotocol_is_valid = False
        self._ws_stream = None

    def select_subprotocol(self, subprotocols):
        self._subprotocol_is_valid = False

        if MQTT_SUBPROTOCOL in subprotocols:
            self._subprotocol_is_valid = True
            return MQTT_SUBPROTOCOL

    def check_origin(self, origin):
        return True

    def open(self):
        if not self._subprotocol_is_valid:
            self.close(1002, 'Invalid subprotocol')
            return

        logger.debug("WebSocket connection opened")
        self._ws_stream = WebSocketStream(self)
        self.target_server.handle_stream(self._ws_stream, self.request.connection.context.address)

    def on_message(self, message):
        if self._ws_stream:
            self._ws_stream.write_upstream(message)

    def on_close(self):
        logger.debug("WebSocket connection closed")
        if self._ws_stream and not self._ws_stream.closed():
            self._ws_stream.close()


class WebSocketStream:
    """
    Duck types IOStream, to serialize WebSocket framed data to a propper byte
    stream.
    """
    def __init__(self, websocket):
        self._websocket = websocket

        self._upstream_buffer = bytearray()

        self._read_bytes_count = None
        self._read_bytes_callback = None

        self._closed = False
        self._close_callback = None

        self.error = None

    def read_bytes(self, num_bytes, callback):
        """
        Reads data sent by the client
        """
        if self._read_bytes_count or self._read_bytes_callback:
            raise Exception('already reading bytes')

        self._read_bytes_count = num_bytes
        self._read_bytes_callback = callback

        if len(self._upstream_buffer) >= num_bytes:
            self._forward_read_bytes()

    @gen.coroutine
    def write(self, data):
        """
        Writes data to be sent to the client
        """
        yield self._websocket.write_message(data, binary=True)

    def write_upstream(self, message):
        """
        Writes data to be sent to the server
        """
        self._upstream_buffer.extend(message)

        if self._read_bytes_count and self._read_bytes_callback and \
                len(self._upstream_buffer) >= self._read_bytes_count:
            self._forward_read_bytes()

    def _forward_read_bytes(self):
        data = self._upstream_buffer[:self._read_bytes_count]
        self._upstream_buffer = self._upstream_buffer[self._read_bytes_count:]

        callback = self._read_bytes_callback

        self._read_bytes_count = None
        self._read_bytes_callback = None

        callback(data)

    def close(self):
        self._closed = True

        if self._close_callback:
            self._close_callback()

    def closed(self):
        return self._closed

    def set_close_callback(self, fn):
        self._close_callback = fn
