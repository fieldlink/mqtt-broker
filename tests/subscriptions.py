import unittest

from broker.subscription import SubscriptionTree, match


class BaseTest(unittest.TestCase):
    @staticmethod
    def assertSubscribed(client, subscriptions, topic, qos=None):
        subscribers = list(subscriptions.get_subscriptions(topic))
        clients = [s.clientid for s in subscribers]

        if client not in clients:
            raise AssertionError('Client "%s" not subscribed in topic "%s"' % (client, topic))

        if qos is not None and not any([s.qos == qos for s in subscribers]):
            found_qos = [s.qos for s in subscribers]
            raise AssertionError('Client "%s" subscription to topic "%s" is not QOS %s (%s found)' % (client, topic, qos, found_qos))

    @staticmethod
    def assertNotSubscribed(client, subscriptions, topic):
        clients = [s.clientid for s in subscriptions.get_subscriptions(topic)]
        if client in clients:
            raise AssertionError('Client "%s" is subscribed in topic "%s"' % (client, topic))


class TestBasic(unittest.TestCase):
    def test_construction1(self):
        from uuid import uuid4
        client = uuid4()

        subs = SubscriptionTree()
        subs.add_subscription(client, 'test', 1)

    def test_construction2(self):
        from uuid import uuid4
        client = uuid4()

        subs = SubscriptionTree()
        subs.add_subscription(client, 'foo/bar', 1)


class TestMaskWildcards(unittest.TestCase):
    def test_valid_subscription_masks(self):
        masks = (
            '#',
            'foo/#',
            '1/data/#',
            '1/control/user/2',
            '1/control/user/+',
            '1/control/+/2',
            '+/control/+/2',
            '+/control/+/#',
            '1/control/devices/wtcsctnfZ-1-2',
            '+',
            '+/',
            '+/#',
            '+/+',
            '+/+/',
            '+/+/#',
            '/+',
            '/+/+',
            '/+/+/#',
            'sport/tennis/player1',
            'sport/tennis/player1/ranking',
            'sport/tennis/player1/score/wimbledon',
            'sport/#',
            'sport/tennis/#',
            '+/tennis/#',
            'sport/+/player1',
            'sport/+/player1/#',
            '/',
            '//',
            '+//',
            '/+/',
            '//+',
            '/#',
            '//#',
            '+//#',
            '/+/#',
            '//+/#',
        )

        subs = SubscriptionTree()
        for mask in masks:
            try:
                subs.add_subscription(1, mask, 1)
            except Exception:
                self.fail("could not subscribe to mask `%s`" % mask)

    def test_invalid_subscription_masks(self):
        masks = (
            '',
            '##',
            '++',
            '#/',
            '/#/+',
            '+#', '#+',
            '+#/', '#+/',
            '/+#', '/#+',
            'sports+',
            'sports#',
            '+sports',
            '#sports',
            'sports/#/',
            'sport/tennis#', 'sport#/tennis',
            '#sport/tennis', 'sport/#tennis',
            'sport/tennis+', 'sport+/tennis',
            'sport/+tennis', '+sport/tennis',
            '++/sport/tennis', 'sport/++/tennis', 'sport/tennis/++',
            '+++/sport/tennis', 'sport/+++/tennis', 'sport/tennis/+++',
            'sport/tennis/#/ranking',
            'sport/tennis/##/ranking',
            '#/sport/tennis/ranking',
            'sport/tennis/ranking/#/',
            '##/sport/tennis/ranking',
            'sport/tennis/ranking/##',
            'sport/tennis/ranking/##/',
        )

        subs = SubscriptionTree()
        for mask in masks:
            with self.assertRaises(Exception, msg=mask):
                subs.add_subscription(1, mask, 1)

    def test_wildcards_in_topic_names(self):
        subs = SubscriptionTree()
        with self.assertRaises(Exception):
            items = list(subs.get_subscriptions('foo/bar/#'))

        with self.assertRaises(Exception):
            items = list(subs.get_subscriptions('foo/bar/+'))
        
        with self.assertRaises(Exception):
            items = list(subs.get_subscriptions('foo/bar/+/'))

        with self.assertRaises(Exception):
            items = list(subs.get_subscriptions('foo/+/bar'))

        with self.assertRaises(Exception):
            items = list(subs.get_subscriptions('foo/bar/+/#'))


class TestMaskMatch(BaseTest):
    def test_exact_match(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, 'foo/bar', 1)

        self.assertSubscribed(1, subs, 'foo/bar')

        self.assertNotSubscribed(1, subs, '/')
        self.assertNotSubscribed(1, subs, 'foobar')
        self.assertNotSubscribed(1, subs, 'foo')
        self.assertNotSubscribed(1, subs, 'foo/')

        self.assertNotSubscribed(1, subs, 'foo/bar/')
        self.assertNotSubscribed(1, subs, '/foo/bar')
        self.assertNotSubscribed(1, subs, 'foo//bar')

        self.assertNotSubscribed(1, subs, 'foo/bar/buzz')
        self.assertNotSubscribed(1, subs, 'buzz/foo/bar')
        self.assertNotSubscribed(1, subs, 'foo/buzz/bar')

    def test_multilevel_match(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar/#", 1)

        self.assertSubscribed(1, subs, 'foo/bar')
        self.assertSubscribed(1, subs, 'foo/bar/')
        self.assertSubscribed(1, subs, 'foo/bar/one')
        self.assertSubscribed(1, subs, 'foo/bar/one/')
        self.assertSubscribed(1, subs, 'foo/bar/one/two')
        self.assertSubscribed(1, subs, 'foo/bar/one/two/')

        self.assertNotSubscribed(1, subs, 'foo')
        self.assertNotSubscribed(1, subs, 'foo/')
        self.assertNotSubscribed(1, subs, 'fo0')
        self.assertNotSubscribed(1, subs, 'fo0/')
        self.assertNotSubscribed(1, subs, '/')

        self.assertNotSubscribed(1, subs, 'fo0/bar')
        self.assertNotSubscribed(1, subs, 'fo0/bar/1')
        self.assertNotSubscribed(1, subs, 'foo/baz')
        self.assertNotSubscribed(1, subs, 'foo/baz/1')

        self.assertNotSubscribed(1, subs, '/foo/bar')
        self.assertNotSubscribed(1, subs, 'zero/foo/bar')
        self.assertNotSubscribed(1, subs, 'zero/foo/bar/')
        self.assertNotSubscribed(1, subs, 'zero/foo/bar/one')

    def test_multilevel_match2(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "#", 1)

        self.assertSubscribed(1, subs, 'foo')
        self.assertSubscribed(1, subs, 'foo/')
        self.assertSubscribed(1, subs, 'foo/bar')
        self.assertSubscribed(1, subs, 'foo/bar/')
        self.assertSubscribed(1, subs, 'foo/bar/one')
        self.assertSubscribed(1, subs, '/')
        self.assertSubscribed(1, subs, '//')

        self.assertSubscribed(1, subs, 'fo0')
        self.assertSubscribed(1, subs, 'fo0/')
        self.assertSubscribed(1, subs, 'fo0/baz')
        self.assertSubscribed(1, subs, 'fo0/baz/')

        self.assertSubscribed(1, subs, '/fo0')
        self.assertSubscribed(1, subs, '/fo0/')
        self.assertSubscribed(1, subs, '/fo0/baz')
        self.assertSubscribed(1, subs, '/fo0/baz/')

    def test_single_level_match(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "+", 1)

        self.assertSubscribed(1, subs, 'foo')
        self.assertSubscribed(1, subs, 'fo0')
        self.assertSubscribed(1, subs, 'bar')
        self.assertSubscribed(1, subs, 'one')

        self.assertNotSubscribed(1, subs, 'foo/')
        self.assertNotSubscribed(1, subs, 'foo/bar')

        self.assertNotSubscribed(1, subs, '/foo')
        self.assertNotSubscribed(1, subs, '/foo/')
        self.assertNotSubscribed(1, subs, '/foo/bar')

        self.assertNotSubscribed(1, subs, '/')
        self.assertNotSubscribed(1, subs, '//')

    def test_single_level_match_middle(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/+/bar", 1)

        self.assertSubscribed(1, subs, 'foo/buzz/bar')
        self.assertSubscribed(1, subs, 'foo/abc/bar')
        self.assertSubscribed(1, subs, 'foo//bar')

        self.assertNotSubscribed(1, subs, 'fo0/buzz/bar')
        self.assertNotSubscribed(1, subs, 'fo0//bar')

        self.assertNotSubscribed(1, subs, 'foo/bar')
        self.assertNotSubscribed(1, subs, 'foo/bar/')
        self.assertNotSubscribed(1, subs, '/foo/bar')
        self.assertNotSubscribed(1, subs, 'foo/one/two/bar')
        self.assertNotSubscribed(1, subs, 'foo/one/bar/')
        self.assertNotSubscribed(1, subs, '/foo/one/bar')

    def test_single_level_match_end(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, 'foo/bar/+', 1)
        self.assertSubscribed(1, subs, 'foo/bar/abc')
        self.assertSubscribed(1, subs, 'foo/bar/xyz')
        self.assertSubscribed(1, subs, 'foo/bar/')

        self.assertNotSubscribed(1, subs, 'foo/bar')
        self.assertNotSubscribed(1, subs, '/foo/bar')
        self.assertNotSubscribed(1, subs, '/foo/bar/')
        self.assertNotSubscribed(1, subs, 'foo/bar/one/')
        self.assertNotSubscribed(1, subs, 'foo/bar/one/two')
        self.assertNotSubscribed(1, subs, 'foo/one/bar/two/')

    def test_single_level_match_beginning(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, '+/foo/bar', 1)
        self.assertSubscribed(1, subs, 'buzz/foo/bar')
        self.assertSubscribed(1, subs, '/foo/bar')

        self.assertNotSubscribed(1, subs, 'foo/bar')
        self.assertNotSubscribed(1, subs, 'foo/bar/one')
        self.assertNotSubscribed(1, subs, 'foo/bar/')
        self.assertNotSubscribed(1, subs, '/foo/bar/')
        self.assertNotSubscribed(1, subs, '/foo/bar/one')
        self.assertNotSubscribed(1, subs, '//foo/bar')

    def test_single_level_multi(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, '+/foo/bar/+', 1)

        self.assertSubscribed(1, subs, 'xyz/foo/bar/abc')
        self.assertSubscribed(1, subs, 'xyz/foo/bar/')
        self.assertSubscribed(1, subs, '/foo/bar/abc')
        self.assertSubscribed(1, subs, '/foo/bar/')

        self.assertNotSubscribed(1, subs, 'foo/bar')
        self.assertNotSubscribed(1, subs, '/foo/bar')
        self.assertNotSubscribed(1, subs, 'foo/bar/')
        self.assertNotSubscribed(1, subs, 'xyz/foo/bar/abc/')
        self.assertNotSubscribed(1, subs, '/xyz/foo/bar/abc')
        self.assertNotSubscribed(1, subs, 'zero/xyz/foo/bar')
        self.assertNotSubscribed(1, subs, '/foo/bar/abc/')
        self.assertNotSubscribed(1, subs, '/foo/bar/abc/123')

    def test_single_level_multi2(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, 'foo/+/bar/+', 1)

        self.assertSubscribed(1, subs, 'foo//bar/')
        self.assertSubscribed(1, subs, 'foo//bar/abc')
        self.assertSubscribed(1, subs, 'foo/xyz/bar/abc')
        self.assertSubscribed(1, subs, 'foo/xyz/bar/')

        self.assertNotSubscribed(1, subs, '/foo//bar/')
        self.assertNotSubscribed(1, subs, 'foo//bar//')

    def test_mixed_match1(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, 'foo/+/bar/#', 1)

        self.assertSubscribed(1, subs, 'foo/xyz/bar')
        self.assertSubscribed(1, subs, 'foo/xyz/bar/')
        self.assertSubscribed(1, subs, 'foo/xyz/bar/abc')
        self.assertSubscribed(1, subs, 'foo/xyz/bar/abc/')
        self.assertSubscribed(1, subs, 'foo//bar')
        self.assertSubscribed(1, subs, 'foo//bar/')
        self.assertSubscribed(1, subs, 'foo//bar/abc')

        self.assertNotSubscribed(1, subs, 'foo/bar')
        self.assertNotSubscribed(1, subs, '/foo/xyz/bar')
        self.assertNotSubscribed(1, subs, 'zero/foo/xyz/bar')

    def test_mixed_match2(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, 'foo/bar/+/#', 1)

        self.assertSubscribed(1, subs, 'foo/bar/xyz')
        self.assertSubscribed(1, subs, 'foo/bar/xyz/')
        self.assertSubscribed(1, subs, 'foo/bar/xyz/abc')
        self.assertSubscribed(1, subs, 'foo/bar/xyz/abc/')
        self.assertSubscribed(1, subs, 'foo/bar/xyz/abc/123')
        self.assertSubscribed(1, subs, 'foo/bar/')
        self.assertSubscribed(1, subs, 'foo/bar//')
        self.assertSubscribed(1, subs, 'foo/bar//abc')
        self.assertSubscribed(1, subs, 'foo/bar//abc/')
        self.assertSubscribed(1, subs, 'foo/bar//abc/123')
        self.assertSubscribed(1, subs, 'foo/bar///')

        self.assertNotSubscribed(1, subs, '/foo/bar')
        self.assertNotSubscribed(1, subs, 'foo/bar')
        self.assertNotSubscribed(1, subs, '/foo/bar/xyz')
        self.assertNotSubscribed(1, subs, 'zero/foo/bar/xyz')

    def test_mixed_match3(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, '+/foo/bar/#', 1)

        self.assertSubscribed(1, subs, 'xyz/foo/bar')
        self.assertSubscribed(1, subs, 'xyz/foo/bar/')
        self.assertSubscribed(1, subs, 'xyz/foo/bar/abc')
        self.assertSubscribed(1, subs, 'xyz/foo/bar/abc/')
        self.assertSubscribed(1, subs, '/foo/bar')
        self.assertSubscribed(1, subs, '/foo/bar/')
        self.assertSubscribed(1, subs, '/foo/bar/abc')
        self.assertSubscribed(1, subs, '/foo/bar/abc/')

        self.assertNotSubscribed(1, subs, 'foo/bar')
        self.assertNotSubscribed(1, subs, 'foo/bar/')
        self.assertNotSubscribed(1, subs, 'foo/bar/xyz')
        self.assertNotSubscribed(1, subs, 'foo/bar/xyz/')
        self.assertNotSubscribed(1, subs, '/xyz/foo/bar')
        self.assertNotSubscribed(1, subs, '/xyz/foo/bar/')
        self.assertNotSubscribed(1, subs, 'zero/xyz/foo/bar')
        self.assertNotSubscribed(1, subs, 'zero/xyz/foo/bar/')

    def test_overwrite(self):
        subs = SubscriptionTree()

        subs.add_subscription(1, "foo/bar/0", 0)
        subs.add_subscription(1, "foo/bar/0", 0)
        self.assertSubscribed(1, subs, "foo/bar/0")

        subs.add_subscription(1, "foo/bar/1/#", 1)
        subs.add_subscription(1, "foo/bar/1/#", 1)
        self.assertSubscribed(1, subs, "foo/bar/1/1")

        subs.add_subscription(1, "foo/bar/2/+", 1)
        subs.add_subscription(1, "foo/bar/2/+", 1)
        self.assertSubscribed(1, subs, "foo/bar/2/1")

        subs.add_subscription(1, "foo/bar/3/+/x", 1)
        subs.add_subscription(1, "foo/bar/3/+/x", 1)
        self.assertSubscribed(1, subs, "foo/bar/3/1/x")


class TestSubUnsub(BaseTest):
    def test_exact(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        self.assertSubscribed(1, subs, "foo/bar")

        subs.remove_subscription(1, "foo/bar")
        self.assertNotSubscribed(1, subs, "foo/bar")

    def test_multilevel(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar/#", 1)
        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(1, subs, "foo/bar/")
        self.assertSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(1, subs, "foo/bar/1/")
        self.assertSubscribed(1, subs, "foo/bar/1/A")
        self.assertSubscribed(1, subs, "foo/bar/1/A/")
        self.assertSubscribed(1, subs, "foo/bar/1/A/X")

        subs.remove_subscription(1, "foo/bar/#")
        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertNotSubscribed(1, subs, "foo/bar/")
        self.assertNotSubscribed(1, subs, "foo/bar/1")
        self.assertNotSubscribed(1, subs, "foo/bar/1/")
        self.assertNotSubscribed(1, subs, "foo/bar/1/A")
        self.assertNotSubscribed(1, subs, "foo/bar/1/A/")
        self.assertNotSubscribed(1, subs, "foo/bar/1/A/X")

    def test_singlelevel(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/+/bar", 1)
        self.assertSubscribed(1, subs, "foo/1/bar")
        self.assertSubscribed(1, subs, "foo/A/bar")

        self.assertNotSubscribed(1, subs, "fo0/1/bar")
        self.assertNotSubscribed(1, subs, "fo0/A/bar")

        subs.remove_subscription(1, "foo/+/bar")
        self.assertNotSubscribed(1, subs, "foo/1/bar")
        self.assertNotSubscribed(1, subs, "foo/A/bar")

        self.assertNotSubscribed(1, subs, "fo0/1/bar")
        self.assertNotSubscribed(1, subs, "fo0/A/bar")

    def test_overwrite(self):
        subs = SubscriptionTree()

        subs.add_subscription(1, "foo/bar/1", 1)
        self.assertSubscribed(1, subs, "foo/bar/1")

        subs.add_subscription(1, "foo/bar/1", 1)
        self.assertSubscribed(1, subs, "foo/bar/1")

        subs.remove_subscription(1, "foo/bar/1")
        self.assertNotSubscribed(1, subs, "fo0/bar/1")

    def test_clients(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")

        subs.add_subscription(2, "foo/bar", 1)
        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar")

        subs.remove_subscription(1, "foo/bar")
        # print(str(subs))
        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar")

        subs.remove_subscription(2, "foo/bar")
        # print(str(subs))
        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertNotSubscribed(2, subs, "foo/bar")

    def test_clients_2(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")

        subs.add_subscription(2, "foo/bar", 1)
        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar")

        subs.remove_client(1)
        # print(str(subs))
        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar")

        subs.remove_subscription(2, "foo/bar")
        # print(str(subs))
        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertNotSubscribed(2, subs, "foo/bar")

    def test_clients_mask(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        subs.add_subscription(2, "foo/bar/#", 1)
        subs.add_subscription(3, "foo/bar/+/1", 1)

        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar/test")
        self.assertSubscribed(3, subs, "foo/bar/test/1")

        subs.remove_subscription(1, "foo/bar")
        # print(str(subs))
        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar/test")
        self.assertSubscribed(3, subs, "foo/bar/test/1")

        subs.remove_subscription(2, "foo/bar/#")
        # print(str(subs))
        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertNotSubscribed(2, subs, "foo/bar/test")
        self.assertSubscribed(3, subs, "foo/bar/test/1")

    def test_clients_mask_2(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        subs.add_subscription(2, "foo/bar/#", 1)
        subs.add_subscription(3, "foo/bar/+/1", 1)

        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar/test")
        self.assertSubscribed(3, subs, "foo/bar/test/1")

        subs.remove_subscription(3, "foo/bar/+/1")
        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar/test")
        self.assertNotSubscribed(3, subs, "foo/bar/test/1")

        subs.remove_subscription(2, "foo/bar/#")
        # print(str(subs))
        self.assertSubscribed(1, subs, "foo/bar")
        self.assertNotSubscribed(2, subs, "foo/bar/test")
        self.assertNotSubscribed(3, subs, "foo/bar/test/1")

    def test_overlap(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar/+", 1)
        subs.add_subscription(1, "foo/bar/foo", 1)

        self.assertSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(1, subs, "foo/bar/foo")

        subs.remove_subscription(1, "foo/bar/+")
        self.assertNotSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(1, subs, "foo/bar/foo")

        subs.add_subscription(1, "foo/bar/+", 1)
        subs.remove_subscription(1, "foo/bar/foo")
        self.assertSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(1, subs, "foo/bar/foo")

    def test_subscription_loss(self):
        subs = SubscriptionTree()
        for i in range(5):
            subs.add_subscription('server', 'upstream/device/+', qos=1)
            subs.add_subscription('server', 'account-x/upstream/client/+', qos=1)
            subs.add_subscription('server', 'account-y/upstream/client/+', qos=1)
            subs.add_subscription('server', 'account-z/upstream/client/+', qos=1)

            self.assertSubscribed('server', subs, 'account-x/upstream/client/client-1')

            for i in range(5):
                subs.remove_client('cli1')
                subs.add_subscription('cli1', 'account-x/downstream/account', qos=1)
                subs.add_subscription('cli1', 'account-x/downstream/role/role-1', qos=1)
                subs.add_subscription('cli1', 'account-x/downstream/client/client-1', qos=1)
                subs.add_subscription('cli1', 'account-x/downstream/data/#', qos=1)
                subs.add_subscription('cli1', 'account-x/downstream/errors/client/client-1', qos=1)
                subs.add_subscription('cli1', 'account-x/downstream/errors/role/role-1', qos=1)
                subs.add_subscription('cli1', 'downstream/user/user-1', qos=1)
                subs.add_subscription('cli1', 'downstream/device/device-1', qos=1)
                subs.add_subscription('cli1', 'downstream/system/#', qos=1)
                subs.add_subscription('cli1', 'downstream/iOS/#', qos=1)

                self.assertSubscribed('server', subs, 'account-x/upstream/client/client-1')
                self.assertSubscribed('cli1', subs, 'account-x/downstream/role/role-1')


class TestMultiSubs(BaseTest):
    def test_exact(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        subs.add_subscription(2, "foo/baz", 1)

        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/baz")

        self.assertNotSubscribed(1, subs, "foo/baz")
        self.assertNotSubscribed(2, subs, "foo/bar")

    def test_single_level(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar/+", 1)
        subs.add_subscription(2, "foo/baz/+", 1)

        self.assertSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(2, subs, "foo/baz/1")

        self.assertNotSubscribed(1, subs, "foo/baz/1")
        self.assertNotSubscribed(2, subs, "foo/bar/1")

    def test_single_level2(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "+/foo/bar", 1)
        subs.add_subscription(2, "+/foo/baz", 1)

        self.assertSubscribed(1, subs, "1/foo/bar")
        self.assertSubscribed(2, subs, "1/foo/baz")

        self.assertNotSubscribed(1, subs, "1/foo/baz")
        self.assertNotSubscribed(2, subs, "1/foo/bar")

    def test_single_level3(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/+/bar", 1)
        subs.add_subscription(2, "foo/+/baz", 1)

        self.assertSubscribed(1, subs, "foo/1/bar")
        self.assertSubscribed(2, subs, "foo/1/baz")

        self.assertNotSubscribed(1, subs, "foo/1/baz")
        self.assertNotSubscribed(2, subs, "foo/1/bar")

    def test_multi_level(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar/#", 1)
        subs.add_subscription(2, "foo/baz/#", 1)

        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/baz")

        self.assertSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(2, subs, "foo/baz/1")

        self.assertNotSubscribed(1, subs, "foo/baz")
        self.assertNotSubscribed(2, subs, "foo/bar")

        self.assertNotSubscribed(1, subs, "foo/baz/1")
        self.assertNotSubscribed(2, subs, "foo/bar/1")

    def test_multi_level2(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar/#", 1)
        subs.add_subscription(2, "foo/bar/1/#", 1)

        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(1, subs, "foo/bar/")
        self.assertSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(1, subs, "foo/bar/1/")
        self.assertSubscribed(1, subs, "foo/bar/1/A")

        self.assertSubscribed(2, subs, "foo/bar/1")
        self.assertSubscribed(2, subs, "foo/bar/1/")
        self.assertSubscribed(2, subs, "foo/bar/1/A")
        self.assertNotSubscribed(2, subs, "foo/bar")
        self.assertNotSubscribed(2, subs, "foo/bar/")


class TestProtocolFeatures(BaseTest):
    def test_replacement(self):
        """
        [MQTT-3.8.4-3] Identical subscriptions MUST be replaced
        """
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        subs.add_subscription(1, "foo/bar", 2)
        self.assertSubscribed(1, subs, "foo/bar", qos=2)

        subs.add_subscription(1, "foo/bar", 1)
        self.assertSubscribed(1, subs, "foo/bar", qos=1)

        subs.add_subscription(1, "foo/bar/A/#", 1)
        self.assertSubscribed(1, subs, "foo/bar/A/1", qos=1)

        subs.add_subscription(1, "foo/bar/A/#", 2)
        self.assertSubscribed(1, subs, "foo/bar/A/X", qos=2)

        subs.add_subscription(1, "foo/+/bar", 1)
        self.assertSubscribed(1, subs, "foo/1/bar", qos=1)

        subs.add_subscription(1, "foo/+/bar", 2)
        self.assertSubscribed(1, subs, "foo/X/bar", qos=2)

    def test_client_remove(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        subs.add_subscription(1, "foo/bar/1", 1)
        subs.add_subscription(1, "foo2/bar/#", 1)
        subs.add_subscription(1, "foo3/+/bar", 1)
        # print(subs)

        self.assertSubscribed(1, subs, "foo/bar")
        self.assertSubscribed(1, subs, "foo/bar/1")
        self.assertSubscribed(1, subs, "foo2/bar/x")
        self.assertSubscribed(1, subs, "foo3/x/bar")

        subs.remove_client(1)
        # print(subs)

        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertNotSubscribed(1, subs, "foo/bar/1")
        self.assertNotSubscribed(1, subs, "foo2/bar/x")
        self.assertNotSubscribed(1, subs, "foo3/x/bar")

        # try to remove once again to see if it breaks
        subs.remove_client(1)

    def test_client_remove_multi_cli(self):
        subs = SubscriptionTree()
        subs.add_subscription(1, "foo/bar", 1)
        subs.add_subscription(1, "foo/bar/1", 1)
        subs.add_subscription(1, "foo2/bar/#", 1)
        subs.add_subscription(1, "foo3/+/bar", 1)

        subs.add_subscription(2, "foo", 1)
        subs.add_subscription(2, "foo/bar", 1)
        subs.add_subscription(2, "foo/bar/1", 1)
        subs.add_subscription(2, "foo/bar/1/2", 1)
        subs.add_subscription(2, "foo/bar/2", 1)
        subs.add_subscription(2, "foo2/bar/#", 1)
        subs.add_subscription(2, "foo2/bar/x/#", 1)
        subs.add_subscription(2, "foo3/+/bar", 1)
        subs.add_subscription(2, "foo3/+/bar/1", 1)
        # print(subs)

        subs.remove_client(1)
        # print(subs)

        self.assertNotSubscribed(1, subs, "foo/bar")
        self.assertNotSubscribed(1, subs, "foo/bar/1")
        self.assertNotSubscribed(1, subs, "foo2/bar/x")
        self.assertNotSubscribed(1, subs, "foo3/x/bar")

        self.assertSubscribed(2, subs, "foo")
        self.assertSubscribed(2, subs, "foo/bar")
        self.assertSubscribed(2, subs, "foo/bar/1")
        self.assertSubscribed(2, subs, "foo/bar/1/2")
        self.assertSubscribed(2, subs, "foo/bar/2")
        self.assertSubscribed(2, subs, "foo2/bar/x")
        self.assertSubscribed(2, subs, "foo2/bar/x/x")
        self.assertSubscribed(2, subs, "foo2/bar/x/x/x")
        self.assertSubscribed(2, subs, "foo3/x/bar")
        self.assertSubscribed(2, subs, "foo3/x/bar/1")


class TestMatchMaskWildcards(unittest.TestCase):
    def test_valid_subscription_masks(self):
        masks = (
            '#',
            'foo/#',
            '1/data/#',
            '1/control/user/2',
            '1/control/user/+',
            '1/control/+/2',
            '+/control/+/2',
            '+/control/+/#',
            '1/control/devices/wtcsctnfZ-1-2',
            '+',
            '+/',
            '+/#',
            '+/+',
            '+/+/',
            '+/+/#',
            '/+',
            '/+/+',
            '/+/+/#',
            'sport/tennis/player1',
            'sport/tennis/player1/ranking',
            'sport/tennis/player1/score/wimbledon',
            'sport/#',
            'sport/tennis/#',
            '+/tennis/#',
            'sport/+/player1',
            'sport/+/player1/#',
            '/',
            '//',
            '+//',
            '/+/',
            '//+',
            '/#',
            '//#',
            '+//#',
            '/+/#',
            '//+/#',
        )

        for mask in masks:
            try:
                match(mask, 'foo/bar')
            except Exception:
                self.fail("could not match to mask `%s`" % mask)

    def test_invalid_subscription_masks(self):
        masks = (
            '',
            '##',
            '++',
            '#/',
            '/#/+',
            '+#', '#+',
            '+#/', '#+/',
            '/+#', '/#+',
            'sports+',
            'sports#',
            '+sports',
            '#sports',
            'sports/#/',
            'sport/tennis#', 'sport#/tennis',
            '#sport/tennis', 'sport/#tennis',
            'sp#ort/tennis', 'sport/ten#nis',
            'sport/tennis+', 'sport+/tennis',
            'sport/+tennis', '+sport/tennis',
            'spo+rt/tennis', 'sport/ten+nis',
            '++/sport/tennis', 'sport/++/tennis', 'sport/tennis/++',
            '+++/sport/tennis', 'sport/+++/tennis', 'sport/tennis/+++',
            'sport/tennis/#/ranking',
            'sport/tennis/##/ranking',
            '#/sport/tennis/ranking',
            'sport/tennis/ranking/#/',
            '##/sport/tennis/ranking',
            'sport/tennis/ranking/##',
            'sport/tennis/ranking/##/',
        )

        for mask in masks:
            with self.assertRaises(Exception, msg=mask):
                match(mask, 'foo/bar')

    def test_wildcards_in_topic_names(self):
        with self.assertRaises(Exception):
            match('/', 'foo/bar/#')

        with self.assertRaises(Exception):
            match('/', 'foo/bar/+')

        with self.assertRaises(Exception):
            match('/', 'foo/bar/+/')

        with self.assertRaises(Exception):
            match('/', 'foo/+/bar')

        with self.assertRaises(Exception):
            match('/', 'foo/bar/+/#')


class TestMatch(unittest.TestCase):
    def test_exact_match(self):
        mask = 'foo/bar'
        self.assertTrue(match(mask, 'foo/bar'))

        self.assertFalse(match(mask, '/'))
        self.assertFalse(match(mask, 'foobar'))
        self.assertFalse(match(mask, 'foo'))
        self.assertFalse(match(mask, 'foo/'))

        self.assertFalse(match(mask, 'foo/bar/'))
        self.assertFalse(match(mask, '/foo/bar'))
        self.assertFalse(match(mask, 'foo//bar'))

        self.assertFalse(match(mask, 'foo/bar/buzz'))
        self.assertFalse(match(mask, 'buzz/foo/bar'))
        self.assertFalse(match(mask, 'foo/buzz/bar'))

    def test_multilevel_match(self):
        mask = "foo/bar/#"

        self.assertTrue(match(mask, 'foo/bar'))
        self.assertTrue(match(mask, 'foo/bar/'))
        self.assertTrue(match(mask, 'foo/bar/one'))
        self.assertTrue(match(mask, 'foo/bar/one/'))
        self.assertTrue(match(mask, 'foo/bar/one/two'))
        self.assertTrue(match(mask, 'foo/bar/one/two/'))

        self.assertFalse(match(mask, 'foo'))
        self.assertFalse(match(mask, 'foo/'))
        self.assertFalse(match(mask, 'fo0'))
        self.assertFalse(match(mask, 'fo0/'))
        self.assertFalse(match(mask, '/'))

        self.assertFalse(match(mask, 'fo0/bar'))
        self.assertFalse(match(mask, 'fo0/bar/1'))
        self.assertFalse(match(mask, 'foo/baz'))
        self.assertFalse(match(mask, 'foo/baz/1'))

        self.assertFalse(match(mask, '/foo/bar'))
        self.assertFalse(match(mask, 'zero/foo/bar'))
        self.assertFalse(match(mask, 'zero/foo/bar/'))
        self.assertFalse(match(mask, 'zero/foo/bar/one'))

    def test_multilevel_match2(self):
        mask = "#"

        self.assertTrue(match(mask, 'foo'))
        self.assertTrue(match(mask, 'foo/'))
        self.assertTrue(match(mask, 'foo/bar'))
        self.assertTrue(match(mask, 'foo/bar/'))
        self.assertTrue(match(mask, 'foo/bar/one'))
        self.assertTrue(match(mask, '/'))
        self.assertTrue(match(mask, '//'))

        self.assertTrue(match(mask, 'fo0'))
        self.assertTrue(match(mask, 'fo0/'))
        self.assertTrue(match(mask, 'fo0/baz'))
        self.assertTrue(match(mask, 'fo0/baz/'))

        self.assertTrue(match(mask, '/fo0'))
        self.assertTrue(match(mask, '/fo0/'))
        self.assertTrue(match(mask, '/fo0/baz'))
        self.assertTrue(match(mask, '/fo0/baz/'))

    def test_single_level_match(self):
        mask = "+"

        self.assertTrue(match(mask, 'foo'))
        self.assertTrue(match(mask, 'fo0'))
        self.assertTrue(match(mask, 'bar'))
        self.assertTrue(match(mask, 'one'))

        self.assertFalse(match(mask, 'foo/'))
        self.assertFalse(match(mask, 'foo/bar'))

        self.assertFalse(match(mask, '/foo'))
        self.assertFalse(match(mask, '/foo/'))
        self.assertFalse(match(mask, '/foo/bar'))

        self.assertFalse(match(mask, '/'))
        self.assertFalse(match(mask, '//'))

    def test_single_level_match_middle(self):
        mask = "foo/+/bar"

        self.assertTrue(match(mask, 'foo/buzz/bar'))
        self.assertTrue(match(mask, 'foo/abc/bar'))
        self.assertTrue(match(mask, 'foo//bar'))

        self.assertFalse(match(mask, 'fo0/buzz/bar'))
        self.assertFalse(match(mask, 'fo0//bar'))

        self.assertFalse(match(mask, 'foo/bar'))
        self.assertFalse(match(mask, 'foo/bar/'))
        self.assertFalse(match(mask, '/foo/bar'))
        self.assertFalse(match(mask, 'foo/one/two/bar'))
        self.assertFalse(match(mask, 'foo/one/bar/'))
        self.assertFalse(match(mask, '/foo/one/bar'))

    def test_single_level_match_end(self):
        mask = 'foo/bar/+'

        self.assertTrue(match(mask, 'foo/bar/abc'))
        self.assertTrue(match(mask, 'foo/bar/xyz'))
        self.assertTrue(match(mask, 'foo/bar/'))

        self.assertFalse(match(mask, 'foo/bar'))
        self.assertFalse(match(mask, '/foo/bar'))
        self.assertFalse(match(mask, '/foo/bar/'))
        self.assertFalse(match(mask, 'foo/bar/one/'))
        self.assertFalse(match(mask, 'foo/bar/one/two'))
        self.assertFalse(match(mask, 'foo/one/bar/two/'))

    def test_single_level_match_beginning(self):
        mask = '+/foo/bar'

        self.assertTrue(match(mask, 'buzz/foo/bar'))
        self.assertTrue(match(mask, '/foo/bar'))

        self.assertFalse(match(mask, 'foo/bar'))
        self.assertFalse(match(mask, 'foo/bar/one'))
        self.assertFalse(match(mask, 'foo/bar/'))
        self.assertFalse(match(mask, '/foo/bar/'))
        self.assertFalse(match(mask, '/foo/bar/one'))
        self.assertFalse(match(mask, '//foo/bar'))

    def test_single_level_multi(self):
        mask = '+/foo/bar/+'

        self.assertTrue(match(mask, 'xyz/foo/bar/abc'))
        self.assertTrue(match(mask, 'xyz/foo/bar/'))
        self.assertTrue(match(mask, '/foo/bar/abc'))
        self.assertTrue(match(mask, '/foo/bar/'))

        self.assertFalse(match(mask, 'foo/bar'))
        self.assertFalse(match(mask, '/foo/bar'))
        self.assertFalse(match(mask, 'foo/bar/'))
        self.assertFalse(match(mask, 'xyz/foo/bar/abc/'))
        self.assertFalse(match(mask, '/xyz/foo/bar/abc'))
        self.assertFalse(match(mask, 'zero/xyz/foo/bar'))
        self.assertFalse(match(mask, '/foo/bar/abc/'))
        self.assertFalse(match(mask, '/foo/bar/abc/123'))

    def test_single_level_multi2(self):
        mask = 'foo/+/bar/+'

        self.assertTrue(match(mask, 'foo//bar/'))
        self.assertTrue(match(mask, 'foo//bar/abc'))
        self.assertTrue(match(mask, 'foo/xyz/bar/abc'))
        self.assertTrue(match(mask, 'foo/xyz/bar/'))

        self.assertFalse(match(mask, '/foo//bar/'))
        self.assertFalse(match(mask, 'foo//bar//'))

    def test_mixed_match1(self):
        mask = 'foo/+/bar/#'

        self.assertTrue(match(mask, 'foo/xyz/bar'))
        self.assertTrue(match(mask, 'foo/xyz/bar/'))
        self.assertTrue(match(mask, 'foo/xyz/bar/abc'))
        self.assertTrue(match(mask, 'foo/xyz/bar/abc/'))
        self.assertTrue(match(mask, 'foo//bar'))
        self.assertTrue(match(mask, 'foo//bar/'))
        self.assertTrue(match(mask, 'foo//bar/abc'))

        self.assertFalse(match(mask, 'foo/bar'))
        self.assertFalse(match(mask, '/foo/xyz/bar'))
        self.assertFalse(match(mask, 'zero/foo/xyz/bar'))

    def test_mixed_match2(self):
        mask = 'foo/bar/+/#'

        self.assertTrue(match(mask, 'foo/bar/xyz'))
        self.assertTrue(match(mask, 'foo/bar/xyz/'))
        self.assertTrue(match(mask, 'foo/bar/xyz/abc'))
        self.assertTrue(match(mask, 'foo/bar/xyz/abc/'))
        self.assertTrue(match(mask, 'foo/bar/xyz/abc/123'))
        self.assertTrue(match(mask, 'foo/bar/'))
        self.assertTrue(match(mask, 'foo/bar//'))
        self.assertTrue(match(mask, 'foo/bar//abc'))
        self.assertTrue(match(mask, 'foo/bar//abc/'))
        self.assertTrue(match(mask, 'foo/bar//abc/123'))
        self.assertTrue(match(mask, 'foo/bar///'))

        self.assertFalse(match(mask, '/foo/bar'))
        self.assertFalse(match(mask, 'foo/bar'))
        self.assertFalse(match(mask, '/foo/bar/xyz'))
        self.assertFalse(match(mask, 'zero/foo/bar/xyz'))

    def test_mixed_match3(self):
        mask = '+/foo/bar/#'

        self.assertTrue(match(mask, 'xyz/foo/bar'))
        self.assertTrue(match(mask, 'xyz/foo/bar/'))
        self.assertTrue(match(mask, 'xyz/foo/bar/abc'))
        self.assertTrue(match(mask, 'xyz/foo/bar/abc/'))
        self.assertTrue(match(mask, '/foo/bar'))
        self.assertTrue(match(mask, '/foo/bar/'))
        self.assertTrue(match(mask, '/foo/bar/abc'))
        self.assertTrue(match(mask, '/foo/bar/abc/'))

        self.assertFalse(match(mask, 'foo/bar'))
        self.assertFalse(match(mask, 'foo/bar/'))
        self.assertFalse(match(mask, 'foo/bar/xyz'))
        self.assertFalse(match(mask, 'foo/bar/xyz/'))
        self.assertFalse(match(mask, '/xyz/foo/bar'))
        self.assertFalse(match(mask, '/xyz/foo/bar/'))
        self.assertFalse(match(mask, 'zero/xyz/foo/bar'))
        self.assertFalse(match(mask, 'zero/xyz/foo/bar/'))
